var Contact = new Class({
	Implements : [Options],
	options: {
		id: '',
		name : '',
		phone : '',
		email: ''
	},	
	initialize : function(options){
		this.setOptions(options);
	},
	toString : function(){
		alert("id : "+this.options.id+"\n name : "+this.options.name+"\n phone : "+this.options.phone+"\n email : "+this.options.email);
	},

	add : function(contact){
		var request = new Request.JSON({
			url: 'php/add.php',
			method: 'post',
			data: JSON.encode(contact.options),
			onSuccess: function(response){
				return "Success";
			},
			onFailure: function(response){
				alert("Failure - "+response);
			},
			onException: function(response){
				alert("Exception - "+response);
			}
		}).send();
	},
	edit : function(contact){
		var request = new Request.JSON({
			url: 'php/edit.php',
			method: 'post',
			data: JSON.encode(contact.options),
			onSuccess: function(response){
				return response;
			},
			onFailure: function(response){
				alert(response);
			},
			onException: function(response){
				alert(response);
			}
		}).send();
	},
	remove : function(id){
		var request = new Request.JSON({
			url: 'php/remove.php',
			data: {'id':id},
			onSuccess: function(response){
				return response;
			},
			onFailure: function(response){
				alert(response);
			},
			onException: function(response){
				alert(response);
			}
		}).send();	
	},
	selectById : function(id){
		var request = new Request.JSON({
			url: 'php/selectById.php',
			data: {'id':id},
			method: 'post',
			onSuccess: function(response){//usar a resposta para popular o form da edit.html
				$$("#edit input[name='id']").set('value', response[0].id);
				$$("#edit input[name='name']").set('value', response[0].name);
				$$("#edit input[name='email']").set('value', response[0].email);
				$$("#edit input[name='phone']").set('value', response[0].phone);
			},
			onFailure: function(response){
				alert(response);
			},
			onException: function(response){
				alert(response);
			}
		}).send();
	},
	selectAll : function(){
		var request = new Request.JSON({
			url: 'php/selectAll.php',
			onSuccess: function(response){
				for(i=0; i < response.length; i++){
					$('tableContacts').appendHTML( 
							'<tr>'+
							 '<td>'+response[i].id+'</td>'+
							 '<td>'+response[i].name+'</td>'+
							 '<td>'+response[i].email+'</td>'+
							 '<td>'+response[i].phone+'</td>'+
							 '<td><p class="link" onclick="loadPageEdit(\'edit.html\','+response[i].id+')">Editar</p></td>'+
							 '<td><p class="link" onclick="shutOff('+response[i].id+')">Remover</p></td>'+
							'</tr>');
				}
			},
			onFailure: function(response){
				alert("Failure "+response);
			},
			onException: function(response){
				alert("Exception "+response);
			}
		}).send();
	},
	selectBy: function(value){
		var request = new Request.JSON({
			url: 'php/selectBy.php',
			data: {'value':value},
			onSuccess: function(response){
				$$('#tableContacts tbody tr').set('html', '');
				for(i=0; i < response.length; i++){					
					$('tableContacts').appendHTML( 
							'<tr>'+
							 '<td>'+response[i].id+'</td>'+
							 '<td>'+response[i].name+'</td>'+
							 '<td>'+response[i].email+'</td>'+
							 '<td>'+response[i].phone+'</td>'+
							 '<td><p class="link" onclick="loadPageEdit(\'edit.html\','+response[i].id+')">Editar</p></td>'+
							 '<td><p class="link" onclick="shutOff('+response[i].id+')">Remover</p></td>'+
							'</tr>');
				}
			},
			onFailure: function(response){
				alert("Failure "+response);
			},
			onException: function(response){
				alert("Exception "+response);
			}
		}).send();
	}
});