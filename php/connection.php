<?php
	try{
		$con = new PDO("mysql:host=localhost;dbname=moogenda", "root", "root");
		$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
     	$con->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);    
	}catch (Exception $error){
		die("Connection failed: ".$error->getMessage());
	}
?>